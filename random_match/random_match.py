#!/usr/bin/python3
# vim: tabstop=4 shiftwidth=4 softtabstop=4
##############################################################################
#  © 2022 cjones
##############################################################################
"""
Parses two excel files. Randomizes the lists. Makes matches
"""
import argparse
from datetime import datetime
from datetime import timedelta
import random
import sys

import pandas


PARSE_FILE_VERSION = '0.0.1'


class DuplicateEmail(Exception):
    pass


class MissingName(Exception):
    pass


class MissingEmail(Exception):
    pass


class MissingLang(Exception):
    pass


class MissingTimeZone(Exception):
    pass


class UnsupportedMode(Exception):
    pass


class ParseFileArgParse():
    """Does the argument parsing for this class"""

    def __init__(self, args):
        self._args = self._parse_args_and_get_filenames(args)
        self._version = PARSE_FILE_VERSION

    def get_version(self):
        """Gets the version of this package"""
        return self._version

    def get_mentor_file_name(self):
        """Gets the name of the mentor file"""
        return self._args.mentor_file_name.name

    def get_mentee_file_name(self):
        """Gets the name of the mentee file"""
        return self._args.mentee_file_name.name

    def get_match_type(self):
        """Gets the match type"""
        return self._args.match_type

    def get_num_headers(self):
        """Gets the number of headers to skip"""
        return self._args.header

    def _parse_args_and_get_filenames(self, args):
        description = 'Match Mentors and Mentees'
        parser = argparse.ArgumentParser(description=description)
        help_text_mentor = 'Relative name of the mentor .xlxs file to parse'
        parser.add_argument('mentor_file_name', type=argparse.FileType('r'),
                            help=help_text_mentor)
        help_text_mentee = 'Relative name of the mentee .xlxs file to parse'
        parser.add_argument('mentee_file_name', type=argparse.FileType('r'),
                            help=help_text_mentee)
        help_msg = 'Match type: (default: simple) \n'
        help_msg = help_msg + ' simple - randomly matches name and e-mail\n'
        help_msg = help_msg + ' lang - same as simple but matches language\n'
        help_msg = help_msg + ' tz - same as lang but also tries to match tz\n'
        parser.add_argument('match_type', choices=['simple', 'lang', 'tz'],
                            nargs='?', default='simple',
                            help=help_msg)
        parser.add_argument('--header', type=int,
                            nargs='?', default=1,
                            help='number of headers to ignore (default: 1)')
        if len(sys.argv) < 3:
            parser.print_help(sys.stderr)
            sys.exit(1)
        return parser.parse_args()


class RandomMatch():
    """Parses the two files into tuples, randomly matches each list together
    and outputs a result"""

    def __init__(self, mentor_filename, mentee_filename,
                 match_type=None, num_headers=None):
        if match_type is None:
            match_type = 'simple'
        if num_headers is None:
            num_headers = 1
        self._mentee_filename = mentee_filename
        self._mentor_filename = mentor_filename
        self._match_type = match_type
        self._num_headers = num_headers
        self._mentors = pandas.read_excel(self._mentor_filename,
                                          header=num_headers)
        self._mentees = pandas.read_excel(self._mentee_filename,
                                          header=num_headers)
        self._no_match_mentor = []
        self._no_match_mentee = []
        self._stats = {}
        self._populate_stats()
        self._verify_tables()
        self._data_frame = pandas.DataFrame()
        # TODO Need to randomize dataframes. We should use pandas.
        self._find_matches()
        self._update_stats()

    def _add_to_data_frame(self, mentor, mentee):
        new_df = pandas.DataFrame({'Mentor e-mail': mentor[0],
                                   'Mentor Name': mentor[1],
                                   'Mentor TZ': mentor[2],
                                   'Mentor Language': mentor[3],
                                   'Mentee e-mail': mentee[0],
                                   'Mentee Name': mentee[1],
                                   'Mentee TZ': mentee[2],
                                   'Mentee Language': mentee[3]},
                                  index=[0])
        self._data_frame = pandas.concat([new_df, self._data_frame.loc[:]],
                                         ignore_index=1)

    def _populate_data_frame_simple(self):
        mentor_names = self._stats['mentor_names']
        mentor_emails = self._stats['mentor_emails']
        mentee_names = self._stats['mentee_names']
        mentee_emails = self._stats['mentee_emails']
        mentor_list = list(zip(mentor_names, mentor_emails))
        mentee_list = list(zip(mentee_names, mentee_emails))
        random.shuffle(mentor_list)
        random.shuffle(mentee_list)
        mentor_mentee_list = list(zip(mentor_list, mentee_list))
        for item in mentor_mentee_list:
            mentor_name = item[0][0]
            mentor_email = item[0][1]
            mentee_name = item[1][0]
            mentee_email = item[1][1]
            new_df = pandas.DataFrame({'Mentor Name': mentor_name,
                                       'Mentor e-mail': mentor_email,
                                       'Mentee Name': mentee_name,
                                       'Mentee e-mail': mentee_email},
                                      index=[0])
            self._data_frame = pandas.concat([new_df, self._data_frame.loc[:]],
                                             ignore_index=1)

    def _populate_data_frame(self):
        cur_mentor_count = self._stats['num_mentor_rows']
        for mentor_index in range(0, cur_mentor_count):
            self._loop_over_mentees(mentor_index, cur_mentor_count, debug=True)

    def _populate_stats(self):
        """Populate the statistic data"""
        self._populate_mentors()
        self._populate_mentees()

    def _update_stats(self):
        """Populate the statistic data"""
        self._stats['num_mentor_mentees'] = len(self._data_frame.index)
        if self._match_type == 'simple':
            num_unmatched_mentors = 0
            num_unmatched_mentees = 0
        else:
            num_unmatched_mentors = len(self._mentors.index)
            num_unmatched_mentees = len(self._mentees.index)
        self._stats['num_unmatched_mentors'] = num_unmatched_mentors
        self._stats['num_unmatched_mentees'] = num_unmatched_mentees

    def _populate_mentees(self):
        """Populate the mentee data"""
        self._stats['num_mentee_rows'] = self._mentees.index.stop
        self._stats['mentee_emails'] = self._mentees.iloc[:, 0]
        self._stats['mentee_names'] = self._mentees.iloc[:, 1]
        self._stats['mentee_tz'] = self._mentees.iloc[:, 2]
        self._stats['mentee_lang'] = self._mentees.iloc[:, 3]

    def _populate_mentors(self):
        """Populate the mentor data"""
        self._stats['num_mentor_rows'] = self._mentors.index.stop
        self._stats['mentor_emails'] = self._mentors.iloc[:, 0]
        self._stats['mentor_names'] = self._mentors.iloc[:, 1]
        self._stats['mentor_tz'] = self._mentors.iloc[:, 2]
        self._stats['mentor_lang'] = self._mentors.iloc[:, 3]

    def _find_matches(self):
        """Find matches for the end user starting with the mentors"""
        if self._match_type == 'simple':
            self._populate_data_frame_simple()
        elif self._match_type in ['lang', 'tz']:
            self._populate_data_frame()
        else:
            msg = "Mode: " + self._match_type + " is not currently supported."
            raise UnsupportedMode(msg)

    def _loop_over_mentees(self, mentor_index, mentor_count, debug=None):
        """Find matches for the mentor with each mentee"""
        match_found = False
        cur_mentee_count = len(self._mentees.index)
        if debug:
            self._loop_debug(mentor_index, mentor_count, cur_mentee_count)
        for mentee_index in range(0, cur_mentee_count):
            mentor_conds, mentee_conds = (
                self._construct_conditions(mentor_index, mentee_index))
            if self._conditions_match(mentor_conds, mentee_conds):
                if debug:
                    print("Match at: " + str(mentor_index) +
                          " mentee: " + str(mentee_index))
                    self._print_match(self._mentors.loc[mentor_index].iat[0],
                                      self._mentors.loc[mentor_index].iat[1],
                                      self._mentees.loc[mentee_index].iat[0],
                                      self._mentees.loc[mentee_index].iat[1],
                                      self._mentees.loc[mentee_index].iat[2],
                                      )
                transpose_mentors = self._mentors.T
                popped_mentor_row = transpose_mentors.pop(mentor_index)
                self._mentors = transpose_mentors.T
                transpose_mentees = self._mentees.T
                popped_mentee_row = transpose_mentees.pop(mentee_index)
                self._mentees = transpose_mentees.T
                self._mentees = self._mentees.reset_index(drop=True)
                self._add_to_data_frame(popped_mentor_row,
                                        popped_mentee_row)
                match_found = True
                break
        if not match_found:
            msg = "!!!!!!Uh oh - No matches for for mentor_index: "
            print(msg + str(mentor_index))

    def _construct_conditions(self, mentor_index, mentee_index):
        mentor_conds = {}
        mentee_conds = {}
        mentor_conds['email'] = self._mentors.loc[mentor_index].iat[0]
        mentee_conds['email'] = self._mentees.loc[mentee_index].iat[0]
        mentor_conds['tz'] = self._mentors.loc[mentor_index].iat[2]
        mentee_conds['tz'] = self._mentees.loc[mentee_index].iat[2]
        mentor_conds['lang'] = self._mentors.loc[mentor_index].iat[3]
        mentee_conds['lang'] = self._mentees.loc[mentee_index].iat[3]
        return mentor_conds, mentee_conds

    def _conditions_match(self, mentor_conds, mentee_conds):
        if self._match_type == 'lang':
            if self._lang_matches(mentor_conds, mentee_conds):
                return True
        if self._match_type == 'tz':
            if (
               self._lang_matches(mentor_conds, mentee_conds) and
               self._tz_matches(mentor_conds, mentee_conds) and
               self._no_match_self(mentor_conds, mentee_conds)):
                return True

    def _lang_matches(self, mentor_conds, mentee_conds):
        if (mentor_conds['lang'] == mentee_conds['lang']):
            return True

    def _tz_matches(self, mentor_conds, mentee_conds):
        if mentor_conds['tz'] == '±00:00':
            mentor_conds['tz'] = '+00:00'
        if mentee_conds['tz'] == '±00:00':
            mentee_conds['tz'] = '+00:00'
        mentor_tz = datetime.strptime(mentor_conds['tz'], '%z')
        mentee_tz = datetime.strptime(mentee_conds['tz'], '%z')

        delta = timedelta(hours=5)
        if (abs(mentor_tz - mentee_tz) < delta):
            return True

    def _no_match_self(self, mentor_conds, mentee_conds):
        if (mentor_conds['email'] != mentee_conds['email']):
            return True

    def _loop_debug(self, mentor_index, mentor_count, cur_mentee_count):
        msg_1 = "Current mentor : " + str(mentor_index)
        msg_2 = "/" + str(mentor_count)
        msg_3 = " Current mentee count: " + str(cur_mentee_count)
        print(msg_1 + msg_2 + msg_3)

    def _print_error(self, column_name, mentor_number, mentee_number):
        msg_1 = "ERROR! You have "
        msg_1 = msg_1 + str(mentor_number) + " mentor " + column_name
        msg_2 = " and you have "
        msg_2 = msg_2 + str(mentee_number) + " mentee " + column_name
        msg = msg_1 + msg_2 + "\n"
        print(msg)
        msg = "These do not equal or there are missing entries. Exiting now!"
        print(msg)

    def _print_match(self, mentor_name, mentor_email, mentee_name,
                     mentee_email, lang=None):
        msg_1 = mentor_name + " <" + mentor_email + "> is matched with: "
        msg_2 = mentee_name + " <" + mentee_email + ">"
        if lang:
            msg_3 = " language is: " + lang
        else:
            msg_3 = "."
        print(msg_1 + msg_2 + msg_3)

    def _print_table(self, table_name, table):
        print(table_name + ": \n")
        print(table)

    def _verify_tables(self):
        num_mentor_rows = self._stats['num_mentor_rows']
        num_mentee_rows = self._stats['num_mentee_rows']
        num_mentor_names = self._stats['mentor_names'].count()
        num_mentee_names = self._stats['mentee_names'].count()
        num_mentor_emails = self._stats['mentor_emails'].count()
        num_mentee_emails = self._stats['mentee_emails'].count()
        num_mentor_lang = self._stats['mentor_lang'].count()
        num_mentee_lang = self._stats['mentee_lang'].count()
        num_mentor_tz = self._stats['mentor_tz'].count()
        num_mentee_tz = self._stats['mentee_tz'].count()
        mentor_names_empty = self._stats['mentor_names'].isnull().values.any()
        mentee_names_empty = self._stats['mentee_names'].isnull().values.any()
        mentor_email_empty = self._stats['mentor_emails'].isnull().values.any()
        mentee_email_empty = self._stats['mentee_emails'].isnull().values.any()
        mentor_lang_empty = self._stats['mentor_lang'].isnull().values.any()
        mentee_lang_empty = self._stats['mentee_lang'].isnull().values.any()
        mentor_tz_empty = self._stats['mentor_tz'].isnull().values.any()
        mentee_tz_empty = self._stats['mentee_tz'].isnull().values.any()

        msg_mentor = "You have " + str(num_mentor_rows) + " mentors, and"
        msg_mentee = " you have " + str(num_mentee_rows) + " mentees.\n"
        print(msg_mentor + msg_mentee)

        if (not self._stats["mentor_emails"].is_unique):
            emails = self._stats["mentor_emails"]
            dup_list = emails[emails.duplicated(keep=False)]
            self._print_error('emails', num_mentor_emails, num_mentee_emails)
            msg = "Duplicate mentor email addresses at {}"
            format_msg = msg.format(str(dup_list.iloc[0]))
            raise DuplicateEmail(format_msg)
        if (not self._stats["mentee_emails"].is_unique):
            emails = self._stats["mentee_emails"]
            dup_list = emails[emails.duplicated(keep=False)]
            self._print_error('emails', num_mentor_emails, num_mentee_emails)
            msg = "Duplicate mentee email addresses at {}"
            format_msg = msg.format(str(dup_list.iloc[0]))
            raise DuplicateEmail(msg)
        if (mentor_names_empty or mentee_names_empty):
            self._print_error('names', num_mentor_names, num_mentee_names)
            msg = "Number of names does not equal or missing entries"
            raise MissingName(msg)
        if (mentor_email_empty or mentee_email_empty):
            self._print_error('emails', num_mentor_emails, num_mentee_emails)
            msg = "Number of emails does not equal or missing entries"
            raise MissingEmail(msg)
        if (self._match_type in ['lang', 'tz'] and
           (mentor_lang_empty or mentee_lang_empty)):
            self._print_error('languages', num_mentor_lang, num_mentee_lang)
            msg = "Number of languages does not equal or missing entries"
            raise MissingLang(msg)
        if (self._match_type == 'tz' and
           (mentor_tz_empty or mentee_tz_empty)):
            self._print_error('Time Zone', num_mentor_tz, num_mentee_tz)
            msg = "Number of time zones does not equal or missing entries"
            raise MissingTimeZone(msg)

    def get_stats(self):
        """Gets the statistics and data"""
        return self._stats

    def print_results(self, match_type):
        """Prints the matches on screen for the end user"""
        print("Here are the matches I've made for you:\n")
        for index, row in self._data_frame.iterrows():
            mentor_name = row['Mentor Name']
            mentor_email = row['Mentor e-mail']
            mentee_name = row['Mentee Name']
            mentee_email = row['Mentee e-mail']
            if match_type != 'simple':
                language = row['Mentee Language']
            else:
                language = None
            self._print_match(mentor_name, mentor_email, mentee_name,
                              mentee_email, language)
        msg_1 = "\nYou have: {} unmatched mentors "
        msg_2 = "and you have: {} unmatched mentees\n"
        format_msg_1 = msg_1.format(str(self._stats['num_unmatched_mentors']))
        format_msg_2 = msg_2.format(str(self._stats['num_unmatched_mentees']))
        print(format_msg_1 + format_msg_2)

    def print_filenames(self):
        """Prints the filenames"""
        print("mentee file name: " + self._mentee_filename)
        print("mentor file name: " + self._mentor_filename)

    def _output_result(self, data_frame, file_name):
        """Output result to file"""
        print("Outputing results to " + file_name)
        data_frame.to_excel(file_name)

    def output_results(self, match_type):
        """Output results to file"""
        output_name = "mentors_mentees_output.xlsx"
        self._output_result(self._data_frame, output_name)
        if match_type != 'simple':
            if len(self._mentors.index):
                output_name = "unmatched_mentors_output.xlsx"
                self._output_result(self._mentors, output_name)
            if len(self._mentees.index):
                output_name = "unmatched_mentees_output.xlsx"
                self._output_result(self._mentees, output_name)

    def print_tables(self):
        """Prints the input tables"""
        self._print_table("Mentors table", self._mentors)
        self._print_table("Mentees table", self._mentees)


def main(args):
    """Main f'n"""
    arg_parser = ParseFileArgParse(args)
    rm = RandomMatch(arg_parser.get_mentor_file_name(),
                     arg_parser.get_mentee_file_name(),
                     arg_parser.get_match_type(),
                     arg_parser.get_num_headers())
    rm.print_results(arg_parser.get_match_type())
    rm.output_results(arg_parser.get_match_type())


if __name__ == "__main__":
    main(sys.argv[1:])
