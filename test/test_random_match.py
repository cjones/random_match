#!/usr/bin/python3
# vim: tabstop=4 shiftwidth=4 softtabstop=4
##############################################################################
#  © 2022 cjones
##############################################################################
"""
test_random_match.py

Creates dummy match files in directories and ensures that they can be read with
the tool and that the output is as expected.

Usage:
> tox -epep8 (for PEP8/flake8 tests)
> tox -epy3
or
> tox -e
"""
import os
import pathlib
import shutil
import unittest

import pandas

from random_match import random_match


class TestRandomMatch(unittest.TestCase):
    '''This class tests the RandomMatch class'''

    @classmethod
    def setUpClass(cls):
        test_dir = str(pathlib.PurePath('./test_data/'))
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)
        cls._dir_list = [
            './test_data/empty_dir/',
            './test_data/empty_file/',
            './test_data/green_path/',
            './test_data/errors/',
        ]
        for dir_name in cls._dir_list:
            os_agnostic_path = str(pathlib.PurePath(dir_name))
            if not os.path.exists(os_agnostic_path):
                os.makedirs(os_agnostic_path)
        cls._copy_csv_to_xlsx('./fake_data/mentor.csv',
                              'test_data/green_path/mentor.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentor_lang.csv',
                              'test_data/green_path/mentor_lang.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentor_tz.csv',
                              'test_data/green_path/mentor_tz.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentee.csv',
                              'test_data/green_path/mentee.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentee_minus_one.csv',
                              'test_data/errors/mentee_minus_one.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentee_missing_email.csv',
                              'test_data/errors/mentee_missing_email.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentee_missing_name.csv',
                              'test_data/errors/mentee_missing_name.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentee_missing_lang.csv',
                              'test_data/errors/mentee_missing_lang.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentor_minus_one.csv',
                              'test_data/errors/mentor_minus_one.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentor_repeat.csv',
                              'test_data/errors/mentor_repeat.xlsx')
        cls._copy_csv_to_xlsx('./fake_data/mentee_repeat.csv',
                              'test_data/errors/mentee_repeat.xlsx')
        pathlib.Path('./test_data/empty_file/file.xlsx').touch()

    @classmethod
    def _copy_csv_to_xlsx(cls, src, dest):
        csv_file = pandas.read_csv(pathlib.Path(src))
        excel_writer = pandas.ExcelWriter(pathlib.Path(dest))
        csv_file.to_excel(excel_writer, index=False)
        excel_writer.close()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(str(pathlib.PurePath('./test_data/')))

    def test_bad_dir(self):
        '''Test where a directory doesn't exist'''
        os_agnostic_path = str(pathlib.PurePath('./foo_dir/'))
        with self.assertRaises(FileNotFoundError):
            random_match.RandomMatch(os_agnostic_path, os_agnostic_path)

    def test_empty_dir(self):
        '''Test a directory that is empty'''
        dir_name = './test_data/empty_dir/'
        os_agnostic_path = str(pathlib.PurePath(dir_name))
        with self.assertRaises(IsADirectoryError):
            random_match.RandomMatch(os_agnostic_path, os_agnostic_path)

    def test_empty_file(self):
        '''Test a file that is empty'''
        file_name = './test_data/empty_file/file.xlsx'
        os_agnostic_path = str(pathlib.PurePath(file_name))
        with self.assertRaises(ValueError):
            random_match.RandomMatch(os_agnostic_path, os_agnostic_path)

    def test_missing_email(self):
        '''Test a file that has a missing e-mail'''
        mentor_filename = './test_data/green_path/mentor.xlsx'
        mentee_filename = './test_data/errors/mentee_missing_email.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        with self.assertRaises(random_match.MissingEmail):
            random_match.RandomMatch(os_agnostic_mentor_file,
                                     os_agnostic_mentee_file)

    def test_missing_name(self):
        '''Test a file that has a missing name'''
        mentor_filename = './test_data/green_path/mentor.xlsx'
        mentee_filename = './test_data/errors/mentee_missing_name.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        with self.assertRaises(random_match.MissingName):
            random_match.RandomMatch(os_agnostic_mentor_file,
                                     os_agnostic_mentee_file)

    def test_missing_lang(self):
        '''Test a file that has a missing language'''
        mentor_filename = './test_data/green_path/mentor.xlsx'
        mentee_filename = './test_data/errors/mentee_missing_lang.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        with self.assertRaises(random_match.MissingLang):
            random_match.RandomMatch(os_agnostic_mentor_file,
                                     os_agnostic_mentee_file,
                                     'lang')

    def test_green_missing_lang_in_simple_mode(self):
        '''Test a file that has a missing language'''
        mentor_filename = './test_data/green_path/mentor.xlsx'
        mentee_filename = './test_data/errors/mentee_missing_lang.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        rm = random_match.RandomMatch(os_agnostic_mentor_file,
                                      os_agnostic_mentee_file)
        stats = rm.get_stats()
        self.assertEqual(stats['num_mentee_rows'], stats['num_mentor_rows'])
        self.assertEqual(len(stats['mentee_names']),
                         len(stats['mentee_emails']))
        self.assertEqual(len(stats['mentor_names']),
                         len(stats['mentor_emails']))
        self.assertEqual(stats['num_unmatched_mentors'], 0)
        self.assertEqual(stats['num_unmatched_mentees'], 0)
        self.assertEqual(stats['num_mentor_mentees'], 50)

    def test_duplicate_mentor_email(self):
        '''Test a mentor file that has a duplicate e-mail'''
        mentor_filename = './test_data/errors/mentor_repeat.xlsx'
        mentee_filename = './test_data/green_path/mentee.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        with self.assertRaises(random_match.DuplicateEmail):
            random_match.RandomMatch(os_agnostic_mentor_file,
                                     os_agnostic_mentee_file)

    def test_duplicate_mentee_email(self):
        '''Test a mentee file that has a duplicate e-mail'''
        mentor_filename = './test_data/green_path/mentor.xlsx'
        mentee_filename = './test_data/errors/mentee_repeat.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        with self.assertRaises(random_match.DuplicateEmail):
            random_match.RandomMatch(os_agnostic_mentor_file,
                                     os_agnostic_mentee_file)

    def test_unsupported_mode(self):
        '''Test a file that has a missing name'''
        mentor_filename = './test_data/green_path/mentor.xlsx'
        mentee_filename = './test_data/green_path/mentee.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        with self.assertRaises(random_match.UnsupportedMode):
            random_match.RandomMatch(os_agnostic_mentor_file,
                                     os_agnostic_mentee_file,
                                     'blat')

    def test_green_path_default(self):
        '''Test two files where everyone matches'''
        pwd = os.getcwd()
        print("pwd: " + pwd)
        mentor_filename = './test_data/green_path/mentor.xlsx'
        mentee_filename = './test_data/green_path/mentee.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        rm = random_match.RandomMatch(os_agnostic_mentor_file,
                                      os_agnostic_mentee_file)
        stats = rm.get_stats()
        self.assertEqual(stats['num_mentee_rows'], stats['num_mentor_rows'])
        self.assertEqual(len(stats['mentee_names']),
                         len(stats['mentee_emails']))
        self.assertEqual(len(stats['mentor_names']),
                         len(stats['mentor_emails']))
        self.assertEqual(stats['num_unmatched_mentors'], 0)
        self.assertEqual(stats['num_unmatched_mentees'], 0)
        self.assertEqual(stats['num_mentor_mentees'], 50)

    def test_green_path(self):
        '''Test two files where everyone matches'''
        pwd = os.getcwd()
        print("pwd: " + pwd)
        mentor_filename = './test_data/green_path/mentor.xlsx'
        mentee_filename = './test_data/green_path/mentee.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        rm = random_match.RandomMatch(os_agnostic_mentor_file,
                                      os_agnostic_mentee_file,
                                      'tz')
        stats = rm.get_stats()
        self.assertEqual(stats['num_mentee_rows'], stats['num_mentor_rows'])
        self.assertEqual(len(stats['mentee_names']),
                         len(stats['mentee_emails']))
        self.assertEqual(len(stats['mentor_names']),
                         len(stats['mentor_emails']))
        self.assertEqual(stats['num_unmatched_mentors'], 0)
        self.assertEqual(stats['num_unmatched_mentees'], 0)
        self.assertEqual(stats['num_mentor_mentees'], 50)

    def test_green_path_extra_mentor_lang(self):
        '''Test two files where not everyone matches due to lang'''
        pwd = os.getcwd()
        print("pwd: " + pwd)
        mentor_filename = './test_data/green_path/mentor_lang.xlsx'
        mentee_filename = './test_data/green_path/mentee.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        rm = random_match.RandomMatch(os_agnostic_mentor_file,
                                      os_agnostic_mentee_file,
                                      'lang')
        stats = rm.get_stats()
        self.assertEqual(stats['num_mentee_rows'], stats['num_mentor_rows'])
        self.assertEqual(len(stats['mentee_names']),
                         len(stats['mentee_emails']))
        self.assertEqual(len(stats['mentor_names']),
                         len(stats['mentor_emails']))
        self.assertEqual(stats['num_unmatched_mentors'], 1)
        self.assertEqual(stats['num_unmatched_mentees'], 1)
        self.assertEqual(stats['num_mentor_mentees'], 49)

    def test_green_path_extra_mentor_tz(self):
        '''Test two files where not everyone matches due to lang'''
        mentor_filename = './test_data/green_path/mentor_tz.xlsx'
        mentee_filename = './test_data/green_path/mentee.xlsx'
        os_agnostic_mentor_file = str(pathlib.PurePath(mentor_filename))
        os_agnostic_mentee_file = str(pathlib.PurePath(mentee_filename))
        rm = random_match.RandomMatch(os_agnostic_mentor_file,
                                      os_agnostic_mentee_file,
                                      'tz')
        stats = rm.get_stats()
        self.assertEqual(stats['num_mentee_rows'], stats['num_mentor_rows'])
        self.assertEqual(len(stats['mentee_names']),
                         len(stats['mentee_emails']))
        self.assertEqual(len(stats['mentor_names']),
                         len(stats['mentor_emails']))
        self.assertEqual(stats['num_unmatched_mentors'], 1)
        self.assertEqual(stats['num_unmatched_mentees'], 1)
        self.assertEqual(stats['num_mentor_mentees'], 49)
