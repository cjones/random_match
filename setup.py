# © 2022 cjones

""" Simple setup to define the entry point for the main module"""

import setuptools

setuptools.setup(
    name='random_match',
    version='0.1',
    install_requires=[],
    packages=setuptools.find_packages(),
    entry_points='''
        [console_scripts]
        random_match=random_match.shell:cli
    ''',
)
