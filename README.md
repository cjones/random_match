# random_match README

This is a program to match two separate groups of people.
One side being a mentor and the other being a mentee.
As long as both spreadsheets contain the same number of rows, this will randomize each list and match it with another in the corresponding list.

Clone this repo.
Execute with the following command:

python random_match/random_match.py <location of mentor file.xlsx> <location of mentee file.xlsx>

The mentor file schema should be the following:

**Mentor Email, Mentor Name, TZ, Language, Number of Mentees** (default to 1 for now).

|Mentor Email|Mentor Name|TZ|Language|Number of Mentees|
|---|---|---|---|---|
|Jane Doe|jane_doe@zyx.com|-06:00|French|1|
|John Doe|john_doe@abc.com|-06:00|French|1|
|Bill Savage|sbill@cba.com|-06:00|French|2|
|Sheila Moore|smoore@ddd.com|-06:00|French|1|

The mentee file schemea should be the following:

**Mentee Email, Mentee Name, TZ, Language**

|Mentee Email|Mentee Name|TZ|Language|
|---|---|---|---|
|Otho Gotzon|ogotzon@abc.com|-06:00|French|
|Allah Thorbjørn|athorbjorn@gz.com|-06:00|French|
|Sayfullah Friedemann|sfriedemann@zyx.com|-06:00|French|
|Ayo Tauno|atauno888@foobar.com|-06:00|French|

## Features

This matches one set of names and e-mail addresses against another set in a randomly mixed order for pairing up indivuduals for any activity.

### General
- `python random_match/random_match.py <location of mentor file.xlsx> <location of mentee file.xlsx>`

### Self testing
- `tox -e pep8` - for pep8/flake8 linting of the python files.
- `tox -e py3` - run unit tests against fake data to ensure things work as desired.

## Requirements

You will need to have python3 installed on your computer
-  [Python3 Installation](https://www.python.org/downloads/)

## Known Issues

None at this time

## Release Notes

- Mentors can only be matched 1:1 with mentees at the moment.
- A new feature will come out that will allow a mentor to be matched with more than one mentee
- A new feature will come out that will allow categorization of mentors and mentees

## Credits

Thanks to the following who have helped contriubte in alpha order:
- `@adickenson`

### 1.0.0

Initial release of random_match

-----------------------------------------------------------------------------------------------------------

### For more information

* [Python3 Installation](https://www.python.org/downloads/)

**Enjoy!**
